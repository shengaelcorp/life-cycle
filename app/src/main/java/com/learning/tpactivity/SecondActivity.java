package com.learning.tpactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import static com.learning.tpactivity.MainActivity.LIFE_CYCLE_CHECKER;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.d(LIFE_CYCLE_CHECKER, "onCreate");
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(LIFE_CYCLE_CHECKER,"onStart");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(LIFE_CYCLE_CHECKER,"onResume");
    }

    protected void onPause(){
        super.onPause();
        Log.d(LIFE_CYCLE_CHECKER,"onPause");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(LIFE_CYCLE_CHECKER,"onDestroy");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d(LIFE_CYCLE_CHECKER,"onRestart");
    }
}
