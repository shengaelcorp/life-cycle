package com.learning.tpactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.time.Instant;

public class MainActivity extends AppCompatActivity {

    public static final String LIFE_CYCLE_CHECKER = "LIFE_CYCLE_CHECKER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(LIFE_CYCLE_CHECKER, "onCreate");

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                launchSecondActivity();
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(LIFE_CYCLE_CHECKER,"onStart");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(LIFE_CYCLE_CHECKER,"onResume");
    }

    protected void onPause(){
        super.onPause();
        Log.d(LIFE_CYCLE_CHECKER,"onPause");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(LIFE_CYCLE_CHECKER,"onDestroy");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d(LIFE_CYCLE_CHECKER,"onRestart");
    }

    public void launchSecondActivity(){
        Intent instant = new Intent(this, SecondActivity.class);
        startActivity(instant);
    }
}
